package entity;

public interface State {

    State getState();

    void changeState(Entity entity);

    NPCstrategy getStrategy(Entity entity);

}
