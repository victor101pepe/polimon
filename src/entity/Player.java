package entity;

// import java.awt.Color;
import java.awt.Graphics2D;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import main.Battle;
import main.GamePanel;
import main.KeyHandler;
import object.Busp;
import object.ItauBike;
import object.Pokebola;
import object.SuperObject;

public class Player extends Entity {
    GamePanel gp;
    KeyHandler keyH;

    public final int screenX; // inicializa a camera
    public final int screenY;

    int temBusp = 0;
    int temBike = 0;
    int temPokebola = 0;
    public int polimonIndex;
    int npcindex;

    public ArrayList<SuperObject> inventory = new ArrayList<>();
    public final int maxInventorySize = 20;

    public Player(GamePanel gp, KeyHandler keyH) {
        super(gp);
        this.gp = gp;
        this.keyH = keyH;

        screenX = gp.screenWidth / 2;
        screenY = gp.screenHeight / 2;

        solidArea = new Rectangle();
        solidArea.x = 8; // pega a partir do pixel (8,0) do player
        solidArea.y = 0;
        solidAreaDefaultX = solidArea.x;
        solidAreaDefaultY = solidArea.y;
        solidArea.width = 32; // a area que permite colisao do player eh dada por 32 pixels em x e em y
        solidArea.height = 32;
        energia = 7;

        setDefaultValues();
        getPlayerImage();
        setItens();
    }

    public void setDefaultValues() {
        worldX = gp.tileSize * 19; // onde o player inicia
        worldY = gp.tileSize * 11;
        direction = "down";
        speed = walk_speed;
        keyH.speedSwitch = true;
        mode = "walk";
        next_images_set = boy_walk;
    }

    public void setItens() {

        inventory.add(new Busp());
        inventory.add(new Pokebola());
        inventory.add(new ItauBike());

    }

    public void getPlayerImage() {

        try {

            File[] boy_walkFile = new File[8];
            File[] boy_bikeFile = new File[8];
            File[] boy_swimFile = new File[8];

            boy_walkFile[0] = new File("src/res/player/boy_up_1.png");

            boy_walkFile[1] = new File("src/res/player/boy_up_2.png");

            boy_walkFile[2] = new File("src/res/player/boy_down_1.png");

            boy_walkFile[3] = new File("src/res/player/boy_down_2.png");

            boy_walkFile[4] = new File("src/res/player/boy_left_1.png");

            boy_walkFile[5] = new File("src/res/player/boy_left_2.png");

            boy_walkFile[6] = new File("src/res/player/boy_right_1.png");

            boy_walkFile[7] = new File("src/res/player/boy_right_2.png");

            boy_bikeFile[0] = new File("src/res/player/bike_up_1.png");

            boy_bikeFile[1] = new File("src/res/player/bike_up_2.png");

            boy_bikeFile[2] = new File("src/res/player/bike_down_1.png");

            boy_bikeFile[3] = new File("src/res/player/bike_down_2.png");

            boy_bikeFile[4] = new File("src/res/player/bike_left_1.png");

            boy_bikeFile[5] = new File("src/res/player/bike_left_2.png");

            boy_bikeFile[6] = new File("src/res/player/bike_right_1.png");

            boy_bikeFile[7] = new File("src/res/player/bike_right_2.png");

            boy_swimFile[0] = new File("src/res/player/swim_up_1.png");

            boy_swimFile[1] = new File("src/res/player/swim_up_2.png");

            boy_swimFile[2] = new File("src/res/player/swim_down_1.png");

            boy_swimFile[3] = new File("src/res/player/swim_down_2.png");

            boy_swimFile[4] = new File("src/res/player/swim_left_1.png");

            boy_swimFile[5] = new File("src/res/player/swim_left_2.png");

            boy_swimFile[6] = new File("src/res/player/swim_right_1.png");

            boy_swimFile[7] = new File("src/res/player/swim_right_2.png");

            boy_walk[0] = ImageIO.read(boy_walkFile[0]);

            boy_walk[1] = ImageIO.read(boy_walkFile[1]);

            boy_walk[2] = ImageIO.read(boy_walkFile[2]);

            boy_walk[3] = ImageIO.read(boy_walkFile[3]);

            boy_walk[4] = ImageIO.read(boy_walkFile[4]);

            boy_walk[5] = ImageIO.read(boy_walkFile[5]);

            boy_walk[6] = ImageIO.read(boy_walkFile[6]);

            boy_walk[7] = ImageIO.read(boy_walkFile[7]);

            boy_bike[0] = ImageIO.read(boy_bikeFile[0]);

            boy_bike[1] = ImageIO.read(boy_bikeFile[1]);

            boy_bike[2] = ImageIO.read(boy_bikeFile[2]);

            boy_bike[3] = ImageIO.read(boy_bikeFile[3]);

            boy_bike[4] = ImageIO.read(boy_bikeFile[4]);

            boy_bike[5] = ImageIO.read(boy_bikeFile[5]);

            boy_bike[6] = ImageIO.read(boy_bikeFile[6]);

            boy_bike[7] = ImageIO.read(boy_bikeFile[7]);

            boy_swim[0] = ImageIO.read(boy_swimFile[0]);

            boy_swim[1] = ImageIO.read(boy_swimFile[1]);

            boy_swim[2] = ImageIO.read(boy_swimFile[2]);

            boy_swim[3] = ImageIO.read(boy_swimFile[3]);

            boy_swim[4] = ImageIO.read(boy_swimFile[4]);

            boy_swim[5] = ImageIO.read(boy_swimFile[5]);

            boy_swim[6] = ImageIO.read(boy_swimFile[6]);

            boy_swim[7] = ImageIO.read(boy_swimFile[7]);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update() {

        if (keyH.upPressed == true || keyH.downPressed == true || keyH.leftPressed == true
                || keyH.rightPressed == true) {
            if (temBike == 1) {
                if (keyH.bikePressed == true && keyH.speedSwitch == true) {
                    keyH.speedSwitch = false;
                    switch (mode) {
                        case "walk":
                            mode = "bike";
                            break;
                        case "bike":
                            mode = "walk";
                        default:
                            break;
                    }
                }
            }

            if (keyH.upPressed == true) {
                direction = "up";
            } else if (keyH.downPressed == true) {
                direction = "down";
            } else if (keyH.leftPressed == true) {
                direction = "left";
            } else if (keyH.rightPressed == true) {
                direction = "right";
            }

            collisionOn = false;
            waterOn = false;
            gp.cChecker.checkTile(this);
            gp.wChecker.checkTile(this);
            int objIndex = gp.cChecker.checkObject(this, true);
            pickUpObject(objIndex); // por enquanto isso é implementado apenas para PEGAR O BUSP
            gp.eHandler.checkEvent(); // checa se é para mudar o mapa (o evento)

            switch (mode) {
                case "walk":
                    speed = walk_speed;
                    next_images_set = boy_walk;
                    break;
                case "bike":
                    speed = bike_speed;
                    next_images_set = boy_bike;
                default:
                    break;
            }

            if (waterOn == true) {
                keyH.speedSwitch = true;
                speed = swim_speed;
                next_images_set = boy_swim;
                mode = "walk";
            }

            // check NPC collision
            npcindex = gp.cChecker.checkEntity(this, gp.npc[gp.currentMap]);
            interactNPC(npcindex);

            // check polimon collision
            polimonIndex = gp.cChecker.checkEntity(this, gp.polimon[gp.currentMap]);
            interactPolimon(polimonIndex);
            if (polimonIndex <= 10) {
                if (gp.polimon[gp.currentMap][polimonIndex].battle == false) {
                    contactPolimon(polimonIndex);
                    demagePolimon(polimonIndex);
                }
            }

            if (collisionOn == false) {
                switch (direction) {
                    case "up":
                        worldY -= speed;
                        break;
                    case "down":
                        worldY += speed;
                        break;
                    case "left":
                        worldX -= speed;
                        break;
                    case "right":
                        worldX += speed;
                        break;

                }
            }

            spriteCounter++;
            if (spriteCounter > 12) {
                if (spriteNum == 1) {
                    spriteNum = 2;
                } else if (spriteNum == 2) {
                    spriteNum = 1;
                }
                spriteCounter = 0;
            }
        }
    }

    // por enquanto isso é implementado apenas para PEGAR O BUSP
    public void pickUpObject(int index) {
        // se tocamos em um objeto
        if (index != 999) {

            String objName = gp.obj[gp.currentMap][index].name;

            switch (objName) {
                case "busp":
                    temBusp++;
                    gp.obj[gp.currentMap][index] = null; // o busp sumirá do mapa assim que o player pegar
                    gp.playSE(1);
                    gp.sMessages.showMessage("voce achou um busp!");
                    break;
                case "pokebola":
                    temPokebola++;
                    gp.obj[gp.currentMap][index] = null; // a pokenola sumirá do mapa assim que o player pegar
                    System.out.println("pokebola: " + temPokebola);
                    gp.playSE(1);
                    gp.sMessages.showMessage("voce achou uma pokebola!");
                    break;
                case "catraca":
                    if (temBusp > 0) {
                        gp.obj[gp.currentMap][index] = null;
                        temBusp--;
                        gp.playSE(3);
                    } else
                        gp.sMessages.showMessage("voce precisa de um busp para passar!");

                    break;
                case "itauBike":
                    if (index == 0) {
                        gp.obj[gp.currentMap][index] = null;
                        gp.sMessages.showMessage(
                                "Você encontrou uma itaú bike!\n Pressione o botão 'B' para usá-la ou descer dela.");
                        temBike = 1;
                        gp.playSE(2);
                    }
                    break;
            }
        }
    }

    public void interactNPC(int index) {
        if (index != 999) {

            if (gp.keyH.enterPressed == true) {
                gp.gameState = gp.dialogueState;
                gp.npc[gp.currentMap][index].speak();
            }

            gp.keyH.enterPressed = false;
        }
    }

    public void interactPolimon(int index) {
        if (index != 999) {

            if (gp.keyH.enterPressed == true) {
                if (gp.polimon[gp.currentMap][index].battle && (gp.polimon[gp.currentMap][index].dialogueIndex == 2)
                        && (temPokebola > 0)) {
                    gp.gameState = gp.battleState;
                    gp.battle = new Battle(gp.player, gp.polimon[gp.currentMap][index]);
                } else {
                    gp.gameState = gp.dialogueState;
                    gp.polimon[gp.currentMap][index].speak();
                }
            }

            gp.keyH.enterPressed = false;
        }
    }

    public void contactPolimon(int index) {
        if (index != 999) {

            if (derrotado == false) {
                energia -= 1;
                derrotado = true;
            }

        }
    }

    public void demagePolimon(int index) {
        if (index != 999) {
            if (gp.polimon[gp.currentMap][index].derrotado == false) {
                gp.polimon[gp.currentMap][index].energia -= 1;
                gp.polimon[gp.currentMap][index].derrotado = true;

                if (gp.polimon[gp.currentMap][index].energia <= 0 && temPokebola > 0) {
                    temPokebola--;
                    gp.polimon[gp.currentMap][index] = null;
                }
            }

        }

    }

    public void draw(Graphics2D g2) {

        BufferedImage image = null;

        switch (direction) {
            case "up":
                if (spriteNum == 1) {
                    image = next_images_set[0];
                }
                if (spriteNum == 2) {
                    image = next_images_set[1];
                }
                break;
            case "down":
                if (spriteNum == 1) {
                    image = next_images_set[2];
                }
                if (spriteNum == 2) {
                    image = next_images_set[3];
                }
                break;
            case "left":
                if (spriteNum == 1) {
                    image = next_images_set[4];
                }
                if (spriteNum == 2) {
                    image = next_images_set[5];
                }
                break;
            case "right":
                if (spriteNum == 1) {
                    image = next_images_set[6];
                }
                if (spriteNum == 2) {
                    image = next_images_set[7];
                }
                break;
        }
        g2.drawImage(image, screenX, screenY, gp.tileSize, gp.tileSize, null);
    }
}
