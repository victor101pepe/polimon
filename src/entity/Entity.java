package entity;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.GamePanel;
import main.UtilityTool;

public class Entity {

    public int worldX, worldY;
    public int speed;
    public int energia;
    public boolean battle;

    public boolean derrotado = false;
    public int contadorDerrota = 0;

    protected NPCstrategy easyStrategy, hardStrategy;

    public final int walk_speed = 4;
    public final int bike_speed = 8;
    public final int swim_speed = 2;

    public GamePanel gp;

    public BufferedImage up1, up2, down1, down2, left1, left2, right1, right2;
    public BufferedImage[] boy_walk = new BufferedImage[8]; // up1, up2, down1, down2, left1, left2, right1, right2; //
    // walk signs
    public BufferedImage[] boy_bike = new BufferedImage[8];// bike_up1, bike_up2, bike_down1, bike_down2, bike_left1,
    // bike_left2, bike_right1, bike_right2; // bike
    public BufferedImage[] boy_swim = new BufferedImage[8];// swim_up1, swim_up2, swim_down1, swim_down2, swim_left1,
    // swim_left2, swim_right1, swim_right2; // bike
    public BufferedImage[] next_images_set = null; // conjuto de imagens (nadar, andar, bike, etc...)

    public String direction;

    public int spriteCounter = 0;
    public int spriteNum = 1;

    // referentes aos dialogos
    String dialogues[] = new String[20];
    int dialogueIndex = 0;

    // define o espaco da entidade que sera sensivel à colisao
    public Rectangle solidArea;
    public int solidAreaDefaultX, solidAreaDefaultY;
    public int actionLockCounter = 0;
    public String mode; // "walk", "swim" e "bike"
    public boolean collisionOn, waterOn;
    protected State easyState, hardState, state;

    public Entity(GamePanel gp) {

        this.gp = gp;
        solidArea = new Rectangle();
        solidArea.x = 8; // pega a partir do pixel (8,0) do player
        solidArea.y = 0;
        solidAreaDefaultX = solidArea.x;
        solidAreaDefaultY = solidArea.y;
        solidArea.width = 32; // a area que permite colisao do player eh dada por 32 pixels em x e em y
        solidArea.height = 32;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void action() {// referente ao strategy
        state.getStrategy(this).action(direction, this, gp.player);
    }

    // Usado para que os personagens tenham falas
    public void speak() {

    }

    public void update() {
        if (gp.keyH.dificult) {
            state = EasyState.instance;
        } else {
            state = HardState.instance;
        }

        action();
        // check TILE COLLISION
        collisionOn = false;
        gp.cChecker.checkTile(this);
        gp.cChecker.checkObject(this, false);
        gp.cChecker.checkPlayer(this);
        gp.wChecker.checkTile(this);
        // gp.cChecker.checkTile(this);
        setDirection(direction);
        if (collisionOn == false) {
            switch (direction) {
                case "up":
                    worldY -= getSpeed();
                    break;
                case "down":
                    worldY += getSpeed();
                    break;
                case "left":
                    worldX -= getSpeed();
                    break;
                case "right":
                    worldX += getSpeed();
                    break;

            }

        }

        if (collisionOn == true) {
            switch (direction) {
                case "up":
                    direction = "down";
                    break;
                case "down":
                    direction = "up";
                    break;
                case "left":
                    direction = "right";
                    break;
                case "right":
                    direction = "left";
                    break;

            }
        }

        spriteCounter++;
        if (spriteCounter > 12) {
            if (spriteNum == 1) {
                spriteNum = 2;
            } else if (spriteNum == 2) {
                spriteNum = 1;
            }
            spriteCounter = 0;
        }

        if (derrotado == true) {
            contadorDerrota++;
            if (contadorDerrota > 40) {
                derrotado = false;
                contadorDerrota = 0;
            }
        }
    }

    public BufferedImage setup(String imagePath) {

        UtilityTool uTool = new UtilityTool();
        BufferedImage image = null;
        File file = new File(imagePath);

        try {
            image = ImageIO.read(file);
            image = uTool.scaleImage(image, gp.tileSize, gp.tileSize);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    public void draw(Graphics2D g2) {

        BufferedImage image = null;
        int screenX = worldX - gp.player.worldX + gp.player.screenX;
        int screenY = worldY - gp.player.worldY + gp.player.screenY;

        if (worldX + gp.tileSize > gp.player.worldX - gp.player.screenX &&
                worldX - gp.tileSize < gp.player.worldX + gp.player.screenX &&
                worldY + gp.tileSize > gp.player.worldY - gp.player.screenY &&
                worldY - gp.tileSize < gp.player.worldY + gp.player.screenY) {

            switch (direction) {
                case "up":
                    if (spriteNum == 1) {
                        image = up1;
                    }
                    if (spriteNum == 2) {
                        image = up2;
                    }
                    break;
                case "down":
                    if (spriteNum == 1) {
                        image = down1;
                    }
                    if (spriteNum == 2) {
                        image = down2;
                    }
                    break;
                case "left":
                    if (spriteNum == 1) {
                        image = left1;
                    }
                    if (spriteNum == 2) {
                        image = left2;
                    }
                    break;
                case "right":
                    if (spriteNum == 1) {
                        image = right1;
                    }
                    if (spriteNum == 2) {
                        image = right2;
                    }
                    break;
            }

            g2.drawImage(image, screenX, screenY, gp.tileSize, gp.tileSize, null);
        }
    }

    public int getSpeed() {
        return state.getStrategy(this).getSpeed();
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

}
