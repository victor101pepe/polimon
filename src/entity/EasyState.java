package entity;

public class EasyState implements State {
    public static State instance = new EasyState();

    @Override
    public State getState() {
        return instance;
    }

    @Override
    public void changeState(Entity entity) {
        entity.state = HardState.instance;
    }

    @Override
    public NPCstrategy getStrategy(Entity entity) {
        return entity.easyStrategy;
    }

}
