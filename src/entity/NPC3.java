package entity;

import main.GamePanel;

public class NPC3 extends Entity {

    public NPC3(GamePanel gp) {
        super(gp);
        energia = 10;
        direction = "down";
        speed = 1;
        easyStrategy = new walk2NPC();
        hardStrategy = new walkNPC();
        state = EasyState.instance;
        battle = false;
    }

    public void getImage() {

        up1 = setup("./src/res/NPC_3/npc3_up1.png");
        up2 = setup("./src/res/NPC_3/npc3_up2.png");
        down1 = setup("./src/res/NPC_3/npc3_down1.png");
        down2 = setup("./src/res/NPC_3/npc3_down2.png");
        left1 = setup("./src/res/NPC_3/npc3_left1.png");
        left2 = setup("./src/res/NPC_3/npc3_left2.png");
        right1 = setup("./src/res/NPC_3/npc3_right1.png");
        right2 = setup("./src/res/NPC_3/npc3_right2.png");

    }

    public void setDialogue() {

        dialogues[0] = "Agora não dá! To atrasado pra sub de MAC0321";
        dialogues[1] = "Não consigo falar agora";
    }

    public void speak() {

        if (dialogues[dialogueIndex] == null) {
            dialogueIndex = 0;
        }

        gp.sMessages.currentMessage = dialogues[dialogueIndex];
        dialogueIndex++;

        // Para colocar o NPC de frente para o jogador:
        switch (gp.player.direction) {

            case "up":
                direction = "down";
                break;
            case "down":
                direction = "up";
                break;
            case "right":
                direction = "left";
                break;
            case "left":
                direction = "right";
                break;
        }

    }

    public void actionNPC() {
        getImage();
        action();
        setDialogue();
    }

}
