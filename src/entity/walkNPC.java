package entity;

import java.util.Random;

public class walkNPC implements NPCstrategy {

    public int actionLockCounter;
    private Random random = new Random();

    public static walkNPC instance = new walkNPC();

    public walkNPC() {
    }

    @Override
    public void action(String direction, Entity entity, Entity playerReference) {
        actionLockCounter++;
        if (actionLockCounter == 300) {
            int i = random.nextInt(100) + 1; // pick a number from 1 to 100

            if (i <= 25) {
                entity.setDirection("up");

            } else if (i <= 50) {
                entity.setDirection("down");

            } else if (i <= 75) {
                entity.setDirection("left");
                ;

            } else if (i <= 100) {

                entity.setDirection("right");
            }

            actionLockCounter = 0;
        }

    }

    @Override
    public int getSpeed() {
        return 2;
    }

}
