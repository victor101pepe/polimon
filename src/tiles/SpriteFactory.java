package tiles;

import java.util.HashMap;

public class SpriteFactory implements AbstractFactory<Skin> {

	HashMap<String,Skin> prototypeMap = new HashMap<>();	//HashMap utilizado pelo padrao prototype

    public SpriteFactory() {
        prototypeMap.put("Padrao", new SkinPadrao());
        prototypeMap.put("Black_white", new SkinBlackWhite());
    }

    /*
     * @brief Método que realiza o clone do hashmap do padrao prototype
     */
    @Override
    public Skin create(String maptype) {
        return prototypeMap.get(maptype).clone();
    }

}

