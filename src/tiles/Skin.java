package tiles;

public interface Skin {
    
	// faz o clone da skin (modo) para o look-and-feel do objeto do jogo
	Skin clone();
	//public void updateSprite();
}
