package tiles;

public interface AbstractFactory<T> {
	
	T create(String type);
}