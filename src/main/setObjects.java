package main;

import object.Busp;
import object.Catraca;
import object.ItauBike;
import object.Pokebola;

// classe que irá colocar todos objetos no devido lugar no mapa
public class setObjects {

    GamePanel gp;

    public setObjects(GamePanel gp) {
        this.gp = gp;
    }

    public void setObject() {

        int mapNum = 0;
        gp.obj[mapNum][0] = new Busp();
        gp.obj[mapNum][0].worldX = 4 * gp.tileSize; // posição X que o objeto vai ficar
        gp.obj[mapNum][0].worldY = 51 * gp.tileSize; // analogo

        // aqui vão as declarações dos outros objetos
        gp.obj[mapNum][1] = new Catraca();
        gp.obj[mapNum][1].worldX = 26 * gp.tileSize;
        gp.obj[mapNum][1].worldY = 64 * gp.tileSize;

        gp.obj[mapNum][2] = new Pokebola();
        gp.obj[mapNum][2].worldX = 46 * gp.tileSize;
        gp.obj[mapNum][2].worldY = 63 * gp.tileSize;

        gp.obj[mapNum][3] = new Pokebola();
        gp.obj[mapNum][3].worldX = 46 * gp.tileSize;
        gp.obj[mapNum][3].worldY = 45 * gp.tileSize;

        gp.obj[mapNum][4] = new Pokebola();
        gp.obj[mapNum][4].worldX = 46 * gp.tileSize;
        gp.obj[mapNum][4].worldY = 33 * gp.tileSize;

        mapNum = 1; // listo todos objetos que quero no mapa 1 agora
        gp.obj[mapNum][0] = new ItauBike();
        gp.obj[mapNum][0].worldX = 6 * gp.tileSize;
        gp.obj[mapNum][0].worldY = 25 * gp.tileSize;

        gp.obj[mapNum][1] = new ItauBike();
        gp.obj[mapNum][1].worldX = 6 * gp.tileSize;
        gp.obj[mapNum][1].worldY = 24 * gp.tileSize;

        gp.obj[mapNum][2] = new ItauBike();
        gp.obj[mapNum][2].worldX = 6 * gp.tileSize;
        gp.obj[mapNum][2].worldY = 23 * gp.tileSize;

        gp.obj[mapNum][3] = new ItauBike();
        gp.obj[mapNum][3].worldX = 6 * gp.tileSize;
        gp.obj[mapNum][3].worldY = 22 * gp.tileSize;

        gp.obj[mapNum][4] = new ItauBike();
        gp.obj[mapNum][4].worldX = 6 * gp.tileSize;
        gp.obj[mapNum][4].worldY = 20 * gp.tileSize;

        gp.obj[mapNum][5] = new ItauBike();
        gp.obj[mapNum][5].worldX = 6 * gp.tileSize;
        gp.obj[mapNum][5].worldY = 21 * gp.tileSize;

        gp.obj[mapNum][6] = new Pokebola();
        gp.obj[mapNum][6].worldX = 46 * gp.tileSize;
        gp.obj[mapNum][6].worldY = 28 * gp.tileSize;

        gp.obj[mapNum][7] = new Pokebola();
        gp.obj[mapNum][7].worldX = 46 * gp.tileSize;
        gp.obj[mapNum][7].worldY = 41 * gp.tileSize;

    }
}
