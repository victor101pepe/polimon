package main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import tiles.SpriteFactory;

public class KeyHandler implements KeyListener {

    GamePanel gp;
    public boolean upPressed, downPressed, leftPressed, rightPressed, enterPressed, bikePressed, speedSwitch,
            mapPressed, mapSwitch, dificultSwitch, dificult, iPressed;
    private SpriteFactory spriteFactory = new SpriteFactory();

    public KeyHandler(GamePanel gp) {
        this.gp = gp;
        dificult = false;

    }

    @Override
    public void keyPressed(KeyEvent e) {

        int code = e.getKeyCode();

        if (gp.gameState == gp.battleState) {
            enterPressed = true;
        }

        if (gp.gameState == gp.titleState) {

            if (code == KeyEvent.VK_W) {
                gp.sMessages.commandNum--;
                if (gp.sMessages.commandNum == -1)
                    gp.sMessages.commandNum = 1;
            }
            if (code == KeyEvent.VK_S) {
                gp.sMessages.commandNum++;
                if (gp.sMessages.commandNum == 2)
                    gp.sMessages.commandNum = 0;
            }
            if (code == KeyEvent.VK_ENTER) {
                if (gp.sMessages.commandNum == 0)
                    gp.gameState = gp.playState;
                else if (gp.sMessages.commandNum == 1)
                    System.exit(0);
            }
        }

        // Ações que podem ser feitas no playstate
        if (gp.gameState == gp.playState) {

            if (code == KeyEvent.VK_W) {
                upPressed = true;
            }
            if (code == KeyEvent.VK_S) {
                downPressed = true;
            }
            if (code == KeyEvent.VK_A) {
                leftPressed = true;
            }
            if (code == KeyEvent.VK_D) {
                rightPressed = true;
            }
            if (code == KeyEvent.VK_B) {
                bikePressed = true;
            }

            // Botão para pausar
            if (code == KeyEvent.VK_P) {
                gp.gameState = gp.pauseState;
            }

            if (code == KeyEvent.VK_ENTER) {
                enterPressed = true;
            }

            if (code == KeyEvent.VK_I) {
                iPressed = true;
                gp.gameState = gp.inventoryState;
            }

            // trocar dificuldade (true = fácil / false = dificil)

        }

        // Modo pausado do jogo
        else if (gp.gameState == gp.pauseState) {
            if (code == KeyEvent.VK_L) {
                if (dificultSwitch) {

                    if (dificult) {
                        dificult = false;
                    } else {
                        dificult = true;
                    }
                    dificultSwitch = false;
                }
            }
            if (code == KeyEvent.VK_P) {
                gp.gameState = gp.playState;
            }
        }

        // Modo dialogo
        else if (gp.gameState == gp.dialogueState) {
            if (code == KeyEvent.VK_ENTER) {
                gp.gameState = gp.playState;
            }
        }

        // desenhar o inventario
        else if (gp.gameState == gp.inventoryState) {
            if (code == KeyEvent.VK_I) {
                gp.gameState = gp.playState;
            }
        }

        // para mudar a aparencia do jogo LOOK AND FEEL
        if (code == KeyEvent.VK_1) { // Sets to the classic look
            setLookAndFeel("Padrao");
            mapPressed = true;
        }
        if (code == KeyEvent.VK_2) { // Sets to the Space look
            setLookAndFeel("Black_white");
            mapPressed = true;
        }
    }

    void setLookAndFeel(String lookName) {
        spriteFactory.create(lookName);
    }

    @Override
    public void keyReleased(KeyEvent e) {

        int code = e.getKeyCode();

        if (code == KeyEvent.VK_W) {
            upPressed = false;
        }
        if (code == KeyEvent.VK_S) {
            downPressed = false;
        }
        if (code == KeyEvent.VK_A) {
            leftPressed = false;
        }
        if (code == KeyEvent.VK_D) {
            rightPressed = false;
        }
        if (code == KeyEvent.VK_B) {
            bikePressed = false;
            speedSwitch = true;
        }
        if (code == KeyEvent.VK_1) {
            mapPressed = false;
            mapSwitch = true;
        }
        if (code == KeyEvent.VK_2) {
            mapPressed = false;
            mapSwitch = true;
        }

        if (code == KeyEvent.VK_L) {
            dificultSwitch = true;
        }

        if (code == KeyEvent.VK_ENTER) {
            enterPressed = false;
        }

        if (code == KeyEvent.VK_I) {
            iPressed = false;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }
}
