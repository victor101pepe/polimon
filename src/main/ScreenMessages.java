package main;

import java.awt.BasicStroke;
// import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.io.IOException;
import object.Pokebola;

public class ScreenMessages {

    Pokebola pokebola = new Pokebola();
    GamePanel gp;
    Graphics2D g2;
    Font textFont, maruMonica;
    public boolean messageOn = false;
    public String message;
    int messageTime = 0;
    public int commandNum = 0;
    public String currentMessage = "";

    public int slotCol = 0;
    public int slotRow = 0;

    public ScreenMessages(GamePanel gp) {
        this.gp = gp;
        textFont = new Font("Sitka Text", Font.ITALIC, 20);
    }

    public void showMessage(String text) {

        message = text;
        messageOn = true;
    }

    public void draw(Graphics2D g2) throws IOException {

        this.g2 = g2;

        // formato das mensagens
        g2.setFont(textFont);
        g2.setColor(Color.white);

        // Tela inicial
        if (gp.gameState == gp.titleState) {
            drawTitleScreen();
        }

        // Modo play
        if (gp.gameState == gp.playState) {

        }

        // Modo pause
        if (gp.gameState == gp.pauseState) {
            drawPauseScreen();
        }

        // Modo dialogo
        if (gp.gameState == gp.dialogueState) {
            drawTextRectangle();
        }

        // Modo inventario
        if (gp.gameState == gp.inventoryState) {
            desenhaInventario();
        }

        // modo batalha
        if (gp.gameState == gp.battleState) {
            drawBattleScreen();
        }

        if (messageOn) {
            g2.drawString(message, gp.tileSize / 2, gp.tileSize); // mensagem e localização na tela
            messageTime++;

            if (messageTime > 180) { // a mensagem fica na tela por 3s
                messageTime = 0;
                messageOn = false;
            }
        }
    }

    private void drawBattleScreen() {
        // desenhar o mapa
        g2.drawImage(gp.battle.battlemap, 0, 0, gp.screenWidth, gp.screenHeight, null);

        // player
        int playerX = gp.tileSize * 3;
        int playerY = gp.screenHeight / 2 + gp.tileSize * 2;
        g2.drawImage(gp.player.boy_walk[6], playerX, playerY, gp.tileSize * 3,
                gp.tileSize * 3,
                null);
        // hearts
        for (int i = 0; i < (gp.battle.player.energia - gp.battle.playerDamage); i++) {
            g2.drawImage(gp.battle.heart, playerX + i * 50, playerY - 60, 50, 50, null);
        }

        // polimon
        int polimonX = gp.screenWidth - gp.tileSize * 5;
        int polimonY = gp.screenHeight / 2 - gp.tileSize * 2;
        g2.drawImage(gp.polimon[gp.currentMap][gp.player.polimonIndex].left1, polimonX, polimonY, gp.tileSize * 3,
                gp.tileSize * 3,
                null);

        for (int i = 0; i < (gp.battle.polimon.energia - gp.battle.polimonDamage); i++) {
            g2.drawImage(gp.battle.heart, polimonX + i * 50, polimonY - 60, 50, 50, null);
        }
        // TITULO
        g2.setFont(g2.getFont().deriveFont(Font.BOLD, 25F));
        g2.setColor(Color.orange);
        String text;
        if (gp.battle.turn)
            text = "SUA VEZ - PRESSIONE 'ENTER' PARA ATACAR";
        else
            text = "VEZ DO POLIMÓN";
        int textX = getXforCenteredText(text);
        g2.drawString(text, textX, gp.tileSize);

    }

    public void drawTitleScreen() throws IOException {

        g2.setColor(new Color(80, 20, 50));
        g2.fillRect(0, 0, gp.screenWidth, gp.screenHeight);

        g2.setFont(g2.getFont().deriveFont(Font.BOLD, 50F));
        String text1 = "POLIMÓN";
        String text2 = "O POKEMÓN DA POLI USP";

        int x1 = getXforCenteredText(text1);
        int y1 = gp.tileSize * 2;

        int x2 = getXforCenteredText(text2);
        int y2 = gp.tileSize * 3;

        // SOMBRA
        g2.setColor(Color.black);
        g2.drawString(text1, x1 + 3, y1 + 3);
        g2.setColor(Color.black);
        g2.drawString(text2, x2 + 3, y2 + 3);

        // TITULO
        g2.setColor(Color.orange);
        g2.drawString(text1, x1, y1);
        g2.setColor(Color.orange);
        g2.drawString(text2, x2, y2);
        // POKEBOLA
        int x3 = gp.screenWidth / 2 - (gp.tileSize * 2);
        int y3 = y2 + 1 * gp.tileSize;
        g2.drawImage(pokebola.image, x3, y3, gp.tileSize * 4, gp.tileSize * 4, null);

        // MENU
        g2.setFont(g2.getFont().deriveFont(Font.BOLD, 30F));
        String item = "PLAY";
        x3 = getXforCenteredText(item);
        y3 += 5 * gp.tileSize;
        g2.drawString(item, x3, y3);
        if (commandNum == 0) {
            g2.drawImage(pokebola.image, x3 - gp.tileSize / 2, y3 - gp.tileSize / 2, gp.tileSize / 2, gp.tileSize / 2,
                    null);
        }

        item = "QUIT";
        x3 = getXforCenteredText(item);
        y3 += gp.tileSize;
        g2.drawString(item, x3, y3);
        if (commandNum == 1) {
            g2.drawImage(pokebola.image, x3 - gp.tileSize / 2, y3 - gp.tileSize / 2, gp.tileSize / 2, gp.tileSize / 2,
                    null);
        }

    }

    public void drawPauseScreen() {

        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 80F));
        String text = "PAUSED";
        int x = getXforCenteredText(text);
        int y = gp.screenHeight / 2;

        g2.drawString(text, x, y);

        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 30F));
        text = "APERTE 'L' PARA TROCOR O JOGO DO MODO " + gp.dificult;
        x = getXforCenteredText(text);
        y += 3 * gp.tileSize;

        g2.drawString(text, x, y);

    }

    public void drawDialogueScreen() {

    }

    public int getXforCenteredText(String text) {
        int lenght = (int) g2.getFontMetrics().getStringBounds(text, g2).getWidth();
        int x = gp.screenWidth / 2 - lenght / 2;
        return x;
    }

    public void drawTextRectangle() {
        // quadrado onde vai ficar as mensagens
        int x = gp.tileSize * 2;
        int y = gp.tileSize / 2;
        int width = gp.screenWidth - (gp.tileSize * 4);
        int height = gp.tileSize * 4;

        drawRectangleMessage(x, y, width, height);

        x += gp.tileSize;
        y += gp.tileSize;
        g2.drawString(currentMessage, x, y);
    }

    public void drawRectangleMessage(int x, int y, int width, int height) {

        Color cor = new Color(128, 128, 128, 210); // rgb e opacidade
        g2.setColor(cor);
        g2.fillRoundRect(x, y, width, height, 35, 35);

        cor = new Color(255, 255, 255);
        g2.setColor(cor);
        g2.setStroke(new BasicStroke(5));
        g2.drawRoundRect(x + 5, y + 5, width - 10, height - 10, 25, 25);
    }

    public void desenhaInventario() {

        int frameX = gp.tileSize * 9;
        int frameY = gp.tileSize;
        int frameWidth = gp.tileSize * 6;
        int frameHeigth = gp.tileSize * 3;
        drawRectangleMessage(frameX, frameY, frameWidth, frameHeigth);

        // slot
        final int slotXstart = frameX + 20;
        final int slotYstart = frameY + 20;
        int slotX = slotXstart;
        int slotY = slotYstart;

        // desenha os itens
        for (int i = 0; i < gp.player.inventory.size(); i++) {

            g2.drawImage(gp.player.inventory.get(i).image, slotX + (i * gp.tileSize), slotY, gp.tileSize, gp.tileSize,
                    null);

            slotX += gp.tileSize;

            if (i == 4 || i == 9 || i == 14) {
                slotX = slotXstart;
                slotY += gp.tileSize;
            }
        }
    }

}
