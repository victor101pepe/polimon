package main;

import entity.Entity;
import entity.NPC3;
import entity.NPC4;
import entity.NPC5;
import entity.NPC_aluno;
import entity.NPC_corredor;
import entity.POLIMON1;
import entity.POLIMON2;
import entity.POLIMON3;
import entity.POLIMON4;
import entity.POLIMON5;
import entity.POLIMON6;
import entity.POLIMON7;

public class AssetSetter {

    GamePanel gp;
    Entity entity;

    public AssetSetter(GamePanel gp, Entity entity) {
        super();
        this.gp = gp;
        this.entity = entity;
    }

    public void setNPC() {

        // primeiro mapa
        int mapNum = 0;
        NPC_aluno npc1 = new NPC_aluno(gp);
        gp.npc[mapNum][0] = npc1;
        gp.npc[mapNum][0].worldX = gp.tileSize * 20;
        gp.npc[mapNum][0].worldY = gp.tileSize * 6;
        npc1.actionNPC();

        NPC_corredor npc2 = new NPC_corredor(gp);
        gp.npc[mapNum][1] = npc2;
        gp.npc[mapNum][1].worldX = gp.tileSize * 30;
        gp.npc[mapNum][1].worldY = gp.tileSize * 10;
        npc2.actionNPC();

        NPC3 npc3 = new NPC3(gp);
        gp.npc[mapNum][2] = npc3;
        gp.npc[mapNum][2].worldX = gp.tileSize * 40;
        gp.npc[mapNum][2].worldY = gp.tileSize * 25;
        npc3.actionNPC();

        NPC4 npc4 = new NPC4(gp);
        gp.npc[mapNum][3] = npc4;
        gp.npc[mapNum][3].worldX = gp.tileSize * 33;
        gp.npc[mapNum][3].worldY = gp.tileSize * 27;
        npc4.actionNPC();

        NPC5 npc5 = new NPC5(gp);
        gp.npc[mapNum][4] = npc5;
        gp.npc[mapNum][4].worldX = gp.tileSize * 35;
        gp.npc[mapNum][4].worldY = gp.tileSize * 5;
        npc5.actionNPC();

        // segundo mapa
        mapNum = 1;
        NPC_aluno npc1_2 = new NPC_aluno(gp);
        gp.npc[mapNum][0] = npc1_2;
        gp.npc[mapNum][0].worldX = gp.tileSize * 20;
        gp.npc[mapNum][0].worldY = gp.tileSize * 21;
        npc1_2.actionNPC();

        NPC_corredor npc2_2 = new NPC_corredor(gp);
        gp.npc[mapNum][1] = npc2_2;
        gp.npc[mapNum][1].worldX = gp.tileSize * 25;
        gp.npc[mapNum][1].worldY = gp.tileSize * 5;
        npc2_2.actionNPC();

        NPC3 npc3_2 = new NPC3(gp);
        gp.npc[mapNum][2] = npc3_2;
        gp.npc[mapNum][2].worldX = gp.tileSize * 32;
        gp.npc[mapNum][2].worldY = gp.tileSize * 43;
        npc3_2.actionNPC();

        NPC4 npc4_2 = new NPC4(gp);
        gp.npc[mapNum][3] = npc4_2;
        gp.npc[mapNum][3].worldX = gp.tileSize * 45;
        gp.npc[mapNum][3].worldY = gp.tileSize * 54;
        npc4_2.actionNPC();

        NPC5 npc5_2 = new NPC5(gp);
        gp.npc[mapNum][4] = npc5_2;
        gp.npc[mapNum][4].worldX = gp.tileSize * 40;
        gp.npc[mapNum][4].worldY = gp.tileSize * 6;
        npc5_2.actionNPC();

    }

    public void setPolimon() {

        // mapa um
        int mapNum = 0;
        POLIMON1 polimon1 = new POLIMON1(gp);
        gp.polimon[mapNum][0] = polimon1;
        gp.polimon[mapNum][0].worldX = gp.tileSize * 31;
        gp.polimon[mapNum][0].worldY = gp.tileSize * 35;
        polimon1.actionNPC();

        POLIMON2 polimon2 = new POLIMON2(gp);
        gp.polimon[mapNum][1] = polimon2;
        gp.polimon[mapNum][1].worldX = gp.tileSize * 24;
        gp.polimon[mapNum][1].worldY = gp.tileSize * 6;
        polimon2.actionNPC();

        POLIMON3 polimon3 = new POLIMON3(gp);
        gp.polimon[mapNum][2] = polimon3;
        gp.polimon[mapNum][2].worldX = gp.tileSize * 31;
        gp.polimon[mapNum][2].worldY = gp.tileSize * 50;
        polimon3.actionNPC();

        POLIMON4 polimon4 = new POLIMON4(gp);
        gp.polimon[mapNum][3] = polimon4;
        gp.polimon[mapNum][3].worldX = gp.tileSize * 28;
        gp.polimon[mapNum][3].worldY = gp.tileSize * 15;
        polimon4.actionNPC();

        // mapa dois
        mapNum = 1;
        POLIMON5 polimon5 = new POLIMON5(gp);
        gp.polimon[mapNum][0] = polimon5;
        gp.polimon[mapNum][0].worldX = gp.tileSize * 31;
        gp.polimon[mapNum][0].worldY = gp.tileSize * 23;
        polimon5.actionNPC();

        POLIMON6 polimon6 = new POLIMON6(gp);
        gp.polimon[mapNum][1] = polimon6;
        gp.polimon[mapNum][1].worldX = gp.tileSize * 27;
        gp.polimon[mapNum][1].worldY = gp.tileSize * 18;
        polimon6.actionNPC();

        POLIMON7 polimon7 = new POLIMON7(gp);
        gp.polimon[mapNum][2] = polimon7;
        gp.polimon[mapNum][2].worldX = gp.tileSize * 32;
        gp.polimon[mapNum][2].worldY = gp.tileSize * 51;
        polimon7.actionNPC();

        POLIMON5 polimon5_2 = new POLIMON5(gp);
        gp.polimon[mapNum][0] = polimon5_2;
        gp.polimon[mapNum][0].worldX = gp.tileSize * 22;
        gp.polimon[mapNum][0].worldY = gp.tileSize * 45;
        polimon5_2.actionNPC();

    }
}
